/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { metaphone } from 'metaphone'

export function compatibility(fun:Function){
  const own = {}.hasOwnProperty
  const fixtures = {
    ablaze: 'ABLS',
    transition: 'TRNSXN',
    astronomical: 'ASTRNMKL',
    buzzard: 'BSRT',
    wonderer: 'WNTRR',
    district: 'TSTRKT',
    hockey: 'HK',
    capital: 'KPTL',
    penguin: 'PNKN',
    garbonzo: 'KRBNS',
    lightning: 'LFTNNK',
    light: 'LFT'
  }
  /** @type {string} */
  let key

  for (key in fixtures) {
    if (own.call(fixtures, key)) {
      fun(key,fixtures)
    }
  }
}

export function metaphoneBoo(params) {
 return metaphone(params)
}